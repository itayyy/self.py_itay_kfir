import datetimeimport
import calendar


if __name__ == '__main__':

    days =  dict(enumerate(calendar.day_name))

    user_date = input("Enter a date in dd/mm/yyyy format: ")
    year, month, day = map(int, user_date.split('/'))
    date = datetime.date(day, month, year)
    weekday = date.weekday()
    print(days[weekday])