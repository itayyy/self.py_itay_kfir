def are_files_equal(file1, file2):
    file1_open = open(file1,"r")
    file2_open = open(file2,"r")
    return file1_open.read()==file2_open.read()

if __name__ == '__main__':
    file1 = "C:\\Users\\user-pc\\Downloads\\file1.txt"
    file2 = "C:\\Users\\user-pc\\Downloads\\file2.txt"
    print(are_files_equal(file1,file2))