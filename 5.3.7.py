
def chocolate_maker(small, big, x):
    big_needed = min(x // 5, big)  # Number of big chocolate cubes needed
    small_needed = x - big_needed * 5  # Number of small chocolate cubes needed

    return small_needed <= small

if __name__ == '__main__':
    print(chocolate_maker(3,1,9))