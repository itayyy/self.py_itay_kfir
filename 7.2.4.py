def seven_boom(end_number):
    for num in range(1, end_number + 1):
        if num % 7 == 0 or '7' in str(num):
            print("BOOM")
        else:
            print(num)

if __name__ == '__main__':
    seven_boom(17)