def shift_left(my_list):
    """
    Shifts a list left by one position.

    Args:
    my_list (list): The list to shift.

    Returns:
    The shifted list.
    """
    if len(my_list) < 2:
        return my_list  # Nothing to shift if the list has less than 2 elements

    first_elem = my_list[0]
    shifted_list = my_list[1:]
    shifted_list.append(first_elem)

    return shifted_list