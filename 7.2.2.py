

def numbers_letters_count(my_str):
    my_list = [0]*2
    for letter in my_str:
        if letter.isdigit():
            my_list[0]+= 1
        elif letter.isalpha():
            my_list[1]+=1
    return my_list
if __name__ == '__main__':
    print(numbers_letters_count("Python 3.6.3"))
