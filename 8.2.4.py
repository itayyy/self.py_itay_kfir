def sort_anagrams(list_of_strings):
    anagram_groups = {}


    for string in list_of_strings:
        sorted_string = ''.join(sorted(string))
        if sorted_string not in anagram_groups:
            anagram_groups[sorted_string] = []

        anagram_groups[sorted_string].append(string)

    return list(anagram_groups.values())


if __name__ == '__main__':
    list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters',
                     'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
    print(sort_anagrams(list_of_words))