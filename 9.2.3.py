def who_is_missing(file_name):
    comma = ","
    file_open = open(file_name,"r")
    content = file_open.read()
    numbers = content.split(comma)
    max_val = max(numbers)

    for num in range(1,int(max_val)):
        if str(num) not in numbers:
            found_num = str(num)
            break
    new_file = open("found.txt","w")
    new_file.write(found_num)
    new_file.close()
    return found_num

if __name__ == '__main__':
    file1 = "C:\\Users\\user-pc\\Downloads\\file1.txt"
    print(who_is_missing(file1))