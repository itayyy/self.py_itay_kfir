def check_valid_input(letter_guessed: str, old_letters_guessed: list) -> bool:
    if len(letter_guessed) >= 2 or not letter_guessed.isalpha or letter_guessed.lower() in old_letters_guessed:
        return False
    return True
if __name__ == '__main__':
    