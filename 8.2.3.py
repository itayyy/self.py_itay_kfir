def mult_tuple(tuple1, tuple2):
    pairs = [(x, y) for x in tuple1 for y in tuple2] + [(y, x) for x in tuple1 for y in tuple2]
    return tuple(pairs)
if __name__ == '__main__':
    first_tuple = (1, 2, 3)
    second_tuple = (4, 5, 6)
    print(mult_tuple(first_tuple, second_tuple))