HANGMAN_PHOTOS  = {"1":"""  x-------x ""","2":"""    x-------x 
    |
    |
    |
    |
    |""","3":"""    x-------x
    |       |
    |       0
    |
    |
    |""","4":"""    x-------x
    |       |
    |       0
    |       |
    |
    |""","5":"""    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""
    ""","6":"""    x-------x
    |       |
    |       0
    |      /|\ 
    |      /
    |""","7":"""  x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""}




def start_picture():
    print("welcome to the game hangman")
    print("""  _    _                                         
     | |  | |                                        
     | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
     |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
     | |  | | (_| | | | | (_| | | | | | | (_| | | | |
     |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                          __/ |                      
                         |___/""")

def choose_word(file_path,index):
    """choses a word from a spesific index in the file given in the file path

        Args:
            file_path (string): path to the file containing the words
            index (int): given by the user and used to select a spesific word

        Returns:
            tuple: a tuple containing the number of words not including duplitcates, and the chosen word
        """

    file_open = open(file_path,"r")
    words = file_open.read().split()
    return words[index]


def show_hidden_word(secret_word, old_letters_guessed):
    """
        Returns a string that represents the state of the secret word with guessed letters revealed.

        Arguments:
        secret_word -- The secret word to be guessed (string).
        old_letters_guessed -- A list of letters that have already been guessed (list of strings).

        Returns:
        A string representing the state of the secret word with guessed letters revealed.
        Letters from the old_letters_guessed list that are in their correct location in secret_word are shown.
        Unrevealed letters are represented by underscores.

        """

    hidden_word = ''
    for letter in secret_word:
        if letter in old_letters_guessed:
            hidden_word += letter
        else:
            hidden_word += '_'
        hidden_word += ' '
    return hidden_word.strip()

def check_valid_input(letter_guessed, old_letters_guessed):
    """
       checkes if the entered string is valid and if it has been guessed before

       Arguments:
       letter_guessed -- the string input from the user
       old_letters_guessed -- a list containing all the letters the user guessed before

       Returns:
       returns True if the input is only one letter that have not been guessed before
       returns False if otherwise
       rtype: bool
       """
    VALID_LETTERS = "abcdefghijklmnopqrstuvwxyz"

    letter_guessed = letter_guessed.lower()
    if len(letter_guessed) > 1:
        return False
    elif letter_guessed not in VALID_LETTERS:
        return False
    elif letter_guessed in old_letters_guessed:
        return False
    else:
        return True


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
      checkes if the old letters list can be updated, if so updates, else it prints "X" and the old letters list sorted in lower caps with " -> " in between
      Arguments:
      letter_guessed -- the string input from the user
      old_letters_guessed -- a list containing all the letters the user guessed before

      Returns:
      returns True if it can
      returns False if otherwise
      rtype: bool
      """

    if check_valid_input(letter_guessed, old_letters_guessed):
        old_letters_guessed.append(letter_guessed.lower())
        return True
    else:
        print("X")
        print("the letter you guessed: ")
        print(" -> ".join(sorted(old_letters_guessed, key=str.lower)[::1]))
        return False


def check_win(secret_word, old_letters_guessed):
    """
       Check if all letters in secret_word are guessed by old_letters_guessed.

       Args:
       secret_word (str): A string representing the secret word.
       old_letters_guessed (list): A list of letters that were already guessed.

       Returns:
       bool: True if all letters in secret_word are guessed by old_letters_guessed, False otherwise.
       """

    for letter in secret_word:
        if letter not in old_letters_guessed:
            return False
    return True

def print_hangman(num_of_tries):
    """prints the hangman pic based on number of failes

        Args:
            num_of_tries (int): number of fails, used as index
        Returns:
        None
        """

    print(HANGMAN_PHOTOS[num_of_tries])

if __name__ == '__main__':
    start_picture() # prints the welcome screen
    file_path= input("please enter file path:") #ask from the user to enter the file path
    index = int(input("please enter index:"))#ask from the user to enter the index of choosen word
    secret_word = choose_word(file_path,index) # save the secret word in val secret_word
    old_letters_guessed = [] # initilizes the list of old letters that were guessed
    print(show_hidden_word(secret_word,old_letters_guessed))  # prints the state of the secret word and how much of it was guessed
    errors_count = 0 # counter for the number of failed guesses
    while True:
        letter = input("enter a letter:") # ask for letter from the user
        while not try_update_letter_guessed(letter,old_letters_guessed):  # as long as the input isn't valid or was guessed, will keep asking for a letter
            letter = input("enter a letter:")# ask for letter from the user

        # its mean the letter is valid
        if letter in secret_word: # check if letter is in the secret_word
            print(show_hidden_word(secret_word, old_letters_guessed)) ## prints the state of the secret word and how much of it was guessed

            if check_win(secret_word, old_letters_guessed): # if it is, checks if the word has been guessed
                print("WIN")  # print WIN message
                break # break the program
        else:
            print(show_hidden_word(secret_word, old_letters_guessed)) ## prints the state of the secret word and how much of it was guessed
            errors_count += 1 # increase the fail counter
            if errors_count == 7: # check if the user dont reached the mistake limit
                print_hangman(str(errors_count)) #print the hangman status
                print("LOSE") # print LOSE message
                break # break the program

            print_hangman(str(errors_count))#print the hangman status


