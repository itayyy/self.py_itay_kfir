
def choose_word(file_path, index):
    file_open = open(file_path,"r")
    words = file_open.read().split()
    num_words = len(words)
    count = 0
    new_list = []

    if index>num_words:
        tuple = words[index-num_words-1]
    else:
        tuple = words[index-1]
    for i in range(1,num_words):
        if words[i] not in new_list:
            count += 1
            new_list.append(words[i])
    new_tuple = (count,tuple)
    return new_tuple


if __name__ == '__main__':
    file1 = "C:\\Users\\user-pc\\Downloads\\file1.txt"
    print(choose_word(file1,15))