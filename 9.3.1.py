def my_mp3_playlist(file_path):
    songs = []
    with open(file_path, 'r') as file:
        for line in file.readlines():
            songs.append(line.strip().split(";"))
    longest_duration = 0
    longest_song = ""
    for song in songs:
        duration_in_seconds = int(song[2].split(':')[0]) * 60 + int(song[2].split(':')[1])
        if duration_in_seconds > longest_duration:
            longest_duration = duration_in_seconds
            longest_song = song[0]

    artist_dict = {}
    for song in songs:
        artist = song[1]
        if artist not in artist_dict:
            artist_dict[artist] = 1
        else:
            artist_dict[artist] += 1

    artist_with_most_songs = max(artist_dict, key=artist_dict.get)

    playlist_analysis = (longest_song, len(songs), artist_with_most_songs)

    return playlist_analysis
if __name__ == '__main__':
    file1 = "C:\\Users\\user-pc\\Downloads\\file1.txt"
    print(my_mp3_playlist(file1))