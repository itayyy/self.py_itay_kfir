

def count_chars(my_str):
    my_dict = {}
    for letter in my_str:
        if letter not in my_dict:
            my_dict[letter] = my_str.count(letter)
    return my_dict

if __name__ == '__main__':
    print(count_chars("abra cadabra"))