def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    if (check_valid_input(letter_guessed, old_letters_guessed)):
        old_letters_guessed.lower().append(letter_guessed)
        return True
    else:
        print("X \n")
        sortedlist = sorted(old_letters_guessed)
        for i, value in enumerate(old_letters_guessed):
            print(sortedlist[i] + "->")
        return False