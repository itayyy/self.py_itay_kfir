def sort_prices(list_of_tuples):
    sorted_list = sorted(list_of_tuples, key=lambda x: x[1], reverse=True)
    #sort by key x[1] for each taple by x[1] 
    return sorted_list
if __name__ == '__main__':
    products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
    print(sort_prices(products))