
def my_mp4_playlist(file_path, new_song):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        if len(lines) < 3:
            lines.extend(['\n'] * (3 - len(lines)))
        lines[2] = new_song + lines[2][lines[2].find(';'):]
    with open(file_path, 'w') as file:
        file.writelines(lines)
    with open(file_path, 'r') as file:
        print(file.read())


if __name__ == '__main__':
    file1 = "C:\\Users\\user-pc\\Downloads\\file1.txt"
    my_mp4_playlist(file1, "Python Love Story")