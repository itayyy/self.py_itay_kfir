def format_list(my_list):
    even_list = my_list[::2]
    last_item = my_list[-1] if len(my_list) % 2 == 0 else ''

    if len(even_list) == 0:
        return last_item

    elif len(even_list) == 1:
        return even_list[0] + ', ' + last_item

    else:
        formatted_list = ', '.join(even_list[:-1]) + ', '  + even_list[-1] + ', and '
        if last_item:
            formatted_list +=  last_item
        return formatted_list


my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
print(format_list(my_list))