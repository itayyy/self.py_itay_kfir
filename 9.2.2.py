def copy_file_content(source, destination):
    file_source = open(source,"r")
    file_destination = open(destination,"w")
    string_copy  = file_source.read()
    file_destination.write(string_copy)

if __name__ == '__main__':
    file1 = "C:\\Users\\user-pc\\Downloads\\file1.txt"
    file2 = "C:\\Users\\user-pc\\Downloads\\file2.txt"
    copy_file_content(file1,file2)