
if __name__ == '__main__':
    HANGMAN_ASCII_ART = """welcome to the game hangman \n   _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/"""
    MAX_TRIES = 6
    print(HANGMAN_ASCII_ART, "\n", MAX_TRIES)
    the_word = input("enter word")
    print(( '_' + " " )* len(the_word))
    guess_letter = input("enter letter :")
    print(guess_letter.lower())
