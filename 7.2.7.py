def arrow(my_char, max_length):
    arr = ""
    for i in range(max_length):
        arr = arr + my_char * (i+1) + '\n'
    for i in range(max_length,-1,-1):
        arr = arr + my_char * (i + 1) + '\n'
    return arr

if __name__ == '__main__':
    print(arrow('*', 5))
