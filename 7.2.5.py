def sequence_del(my_str):
    new_str = ''
    for letter in my_str:
        if not letter in new_str:
            new_str+= letter
    return new_str

if __name__ == '__main__':
    print(sequence_del("SSSSsssshhhh"))